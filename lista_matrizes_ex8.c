#include <stdlib.h>
#include <stdio.h>

int main(){

int notas[5][3];
char nomes[5][20];
float mediaAluno,media1,media2,media3;
int i,j,avaliacao,indice,maior;
//----entrada das informações dos alunos e notas
for(i=0;i<5;i++){
	for(j=0;j<3;j++){
	printf("digite a nota %d do aluno\n",j);
	scanf("%d",&notas[i][j]);
	}
	printf("digite o nome do aluno[%d]\n",i);
	scanf("%s[^\n]",nomes[i]);
}

//--Apresentando a tabela de forma visual
for(i=0;i<5;i++){
	printf("ALUNO: [%s]\t",nomes[i]);
	for(j=0;j<3;j++){
	printf("  %d\t ",notas[i][j]);
	}
	printf("\n");
}


//---questao E: apresentar o nome de cada aluno e a média de suas nota
for(i=0;i<5;i++){
	printf("ALUNO: %s\n",nomes[i]);
	mediaAluno =0;
	for(j=0;j<3;j++){
		mediaAluno = mediaAluno + notas[i][j];
	}
	printf("MEDIA DO ALUNO: %f\n",(mediaAluno/3));
}

//----questão D: do calculo das medias por avaliação 
media1=0;
media2=0;
media3=0;
for(i=0;i<5;i++){
	media1 = media1+notas[i][0];
	media2 = media3+notas[i][1];
	media3 = media3+notas[i][2];
}
printf("Médias das avaliações eh: M1=%f M2=%f M3=%f\n",(media1/5),(media2/5),(media3/5));

//----questão B: O usuário deverá informar qual avaliação e o algoritmo deverá retornar a maior nota da turma para a avaliação informada e qual o nome do aluno que tirou essa nota
printf("Digite a avaliação que deseja encontrar a nota mais alta\n");
scanf("%d",&avaliacao);
maior = notas[0][avaliacao];
indice =0;
for(i=0;i<5;i++){
	if(maior < notas[i][avaliacao]){
		maior = notas[i][avaliacao];
		indice = i;
	}
}
printf("Maior nota da avaliação %d eh %d do aluno: %s\n",avaliacao,maior,nomes[indice]);

//--questão C: O usuário deverá informar a avaliação e o aluno (número do mesmo) e deverá ser apresentada a nota correspondente
printf("Digite a avaliação (0-2) e o número do aluno (0-4) a ser apresentado a nota:\n");
scanf("%d",&avaliacao);
scanf("%d",&indice);
printf("Nota do aluno %s na avaliação %d eh %d \n",nomes[indice],avaliacao,notas[indice][avaliacao]);

return 0;
}
