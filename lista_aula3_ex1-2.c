#include <stdio.h>
#include <stdlib.h>

int main(){
	char nome[20];
	int opcao;
	
	printf("digite o seu nome\n");
	scanf("%s",nome);
	printf("digite 1-professor,2-servidor,3-aluno,4-bolsista\n");
	scanf("%d",&opcao);
	switch(opcao){
		case 1:
				printf("%s professor paga R$8,50\n",nome);
				break;
		case 2:
				printf("%s servidor paga R$8,50\n",nome);
				break;
		case 3:
				printf("%s aluno paga R$2,50\n",nome);
				break;
		case 4:
				printf("%s bolsista não paga\n",nome);
				break;
		default:
				printf("%s por favor digite um valor entre 1-4\n",nome);
				break;
	}
	return 0;
}
