#include <stdio.h>
#include <stdlib.h>

void fatorialProc(int numero); //como procedimento
int  fatorialFunc(int numero); //como função

int main(){
int qualquer,res;
printf("digite um valor\n");
scanf("%d",&qualquer);
res=fatorialFunc(qualquer);
printf("resultado da funcao eh:%d\n",res);
fatorialProc(qualquer);
return 0;
}

int  fatorialFunc(int numero){ //como função
	int resultado;
	resultado =1;
	while(numero > 0){
		resultado = resultado*numero;
		numero = numero-1;
	}
	return resultado;
}

void fatorialProc(int numero){ //como procedimento
	int resultado;
	resultado =1;
	while(numero > 0){
		resultado = resultado*numero;
		numero = numero-1;
	}
	printf("resultado eh: %d\n",resultado);
	return;
}
