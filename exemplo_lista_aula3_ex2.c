#include <stdlib.h>
#include <stdio.h>

int main(){
	char operacao;
	float valor1,valor2,resultado;
	printf("escolha a operacao\n");
	printf("s-soma,d-divisao,-m-multiplicacao,n-subtracao\n");
	scanf("%c",&operacao);
	printf("digite os valores\n");
	scanf("%f",&valor1);
	scanf("%f",&valor2);
	switch(operacao){
		case 's':
		case 'S':
					resultado = valor1+valor2;
					printf("o resultado eh %f",resultado);
					break;
		case 'd':
		case 'D':
					if(valor2 > 0){
						resultado = valor1/valor2;
						printf("o resultado eh %f",resultado);
					}
					else{
						printf("ERRO DIVISAO POR ZERO\n");
					}
					break;
		case 'm':
		case 'M':
					resultado = valor1 * valor2;
					printf("o resultado eh %f",resultado);
					break;
		case 'n':
		case 'N':
					resultado = valor1 - valor2;
					printf("o resultado eh %f",resultado);
					break;
		default:
				printf("operacao invalida\n");		
	}
return 0;
}
