#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(){

double valor,resultado;

valor = 0;
while(valor>=0){
 printf("######PROGRAMA DA RAIZ QUADRADA#######\n")	;
 printf("Digite um valor:\n");	
 scanf("%lf",&valor);
 resultado = sqrt(valor);
 if(valor < 0){
	printf("valor negativo, programa irá finalizar\n");
 }
 else{
	printf("a raiz quadrada de %lf eh %lf\n",valor,resultado);
 }
}
return 0;
}
