/*Algoritmo Exemplo
Var M[10,10]: inteiro;
i, j, SomaDP: inteiro;
Inicio
SomaDP:= 0;
Para i:= 0 até 10 faça
 Para j:=0 até 10 faça
	Escreva (“Digite o n da linha”, i, “e coluna”, j)
	Leia M[i,j]
	Se (i==j) então
		SomaDP:=SomaDP+M(i,j)
	FimSe
  FimPara
FimPara
Escreva (“Soma da diagonal principal:”, SomaDP)
Fim
*/

#include <stdlib.h>
#include <stdio.h>

int main(){
int i,j,somaDp=0;
int m[5][5];
for(i=0;i<5;i++){
	for(j=0;j<5;j++){
		printf("Digite o n da linha %d e coluna %d\n",i,j);
		scanf("%d",&m[i][j]);
		if(i==j){
			somaDp = somaDp + m[i][j];
		}		
	}
}
printf("Soma da diagonal principal:%d", somaDp);
return 0;
}
